﻿using Lecture05_Lab.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture05_Lab
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = DummyDataSource.Movies;

            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.Phone.UI.Input.HardwareButtons")) 
            {
                // But this is only for UWP AND Mobile	
                Windows.Phone.UI.Input.HardwareButtons.CameraPressed += this.Camera_Pressed;
            }


            sendToast("Hello from UWP");
        }

        private void Camera_Pressed(object sender, CameraEventArgs e)
        {
            sendToast("Camera button clicked"); ;
        }

        private void sendToast(string content)
        {
            // build toast
            var template = ToastTemplateType.ToastText01;
            var xml = ToastNotificationManager.GetTemplateContent(template);
            xml.DocumentElement.SetAttribute("launch", "Args");

            // set value
            var text = xml.CreateTextNode(content);
            var elements = xml.GetElementsByTagName("text");
            elements[0].AppendChild(text);

            // show toast
            var toast = new ToastNotification(xml);
            var notifier = ToastNotificationManager.CreateToastNotifier();
            notifier.Show(toast);

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AddMovieView),this.DataContext);
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            string content = $@"
                    <tile>
                        <visual>
 
                            <binding template='TileSmall' hint-textStacking='center'>
                                <text hint-align='center'>Mon</text>
                                <text hint-align='center' hint-style='body'>22</text>
                            </binding>
 
                           <binding template='TileMedium' branding='name' displayName='Monday 22'>
                               <text hint-wrap='true' hint-maxLines='2'>Teaching Windows Apps</text>
                               <text hint-style='captionSubtle'>Fri: 9:00 AM</text>
                           </binding>
                        </visual>
                    </tile>";

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(content);
            var updator = TileUpdateManager.CreateTileUpdaterForApplication();
            var notification = new TileNotification(xml);
            updator.Update(notification);

        }
    }
}
